#' check existence results for squareControl
#'
#' @param repository the repository
#'
#' @return [logical]`(1)` TRUE, if any result exists
#'
#' @export
#'
doResultsExist <- function(repository) {
  UseMethod("doResultsExist")
}
