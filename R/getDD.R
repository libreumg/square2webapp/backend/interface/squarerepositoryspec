#' get the data dictionary
#'
#' @param repository the object of the implementation
#'
#' @return dd as `tibble`
#' @export
getDD <- function(repository) {
  UseMethod("getDD")
}
