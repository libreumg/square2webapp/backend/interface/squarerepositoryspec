#' send message "renderer started"
#'
#' @param repository the object of the implementation
#'
#' @export
sendMsgRendrStarted <- function(repository){
  UseMethod("sendMsgRendrStarted")
}
