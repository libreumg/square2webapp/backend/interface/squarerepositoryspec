# squarerepositoryspec 0.1.1

  - corrected function signature

# squarerepositoryspec 0.1.0

  - add get user reports function
  - internal code cleanup

# squarerepositoryspec 0.0.6

  - Added a callback for closing repository objects.
  - Added a callback for calling this new callback when the R session
    stops automatically.

# squarerepositoryspec 0.0.5.5

  - Added a `NEWS.md` file to track changes to the package.
  - Fixed some issues with documentation and spelling.

